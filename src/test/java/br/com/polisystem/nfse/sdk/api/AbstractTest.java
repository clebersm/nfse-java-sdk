package br.com.polisystem.nfse.sdk.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public abstract class AbstractTest {

    protected String localhost = "https://192.168.80.157:44364";
    //protected WireMockServer wireMockServer;

/*
    public WireMockServer initWireMockServer(){
        wireMockServer = new WireMockServer(wireMockConfig()
                .bindAddress(localhost)
                .dynamicPort()
                .dynamicHttpsPort()
        );
        wireMockServer.start();

        return wireMockServer;
    }
*/

/*
    public void stopWireMockServer() {
        if (wireMockServer != null) {
            wireMockServer.stop();
        }
    }
*/

    public String toJSON(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {

            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getBaseURL(){
        return localhost;
    }
}

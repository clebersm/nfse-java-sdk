package br.com.polisystem.nfse.sdk.api;

import br.com.polisystem.nfse.sdk.messages.RpsRequest;
import br.com.polisystem.nfse.sdk.messages.RpsResponse;

import org.joda.time.DateTime;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

public class RpsTest extends AbstractTest {

    long ID = 31;

    public RpsRequest createRequestSalvador() {

        RpsRequest result = new RpsRequest();
        result.setNdg_idPolinex(491);
        result.setPrestador_nome("ALPHA PITUBA");
        result.setNdg_paymvId("987897-7897-7897-7897-7897987h989");
        result.setNdg_unitId("8bcaff6f-01d0-4895-89da-6db9202547c1");
        result.setRps_dataEmissao(new DateTime());
        result.setRps_dataCompetencia(new DateTime());
        result.setRps_discriminacaoServico("MENSALIDADE MUSCULAÇÃO");
        result.setRps_valorServicos(BigDecimal.valueOf(10.05d));
        result.setPrestador_cnpj("32674541000191");
        result.setPrestador_inscricaoMunicipal("06913600109");
        //result.setPrestador_alicota(BigDecimal.valueOf(0.05d));
        result.setPrestador_naturezaOperacao(1);
        result.setPrestador_optanteSimplesNacional(2);
        result.setPrestador_itemListaServico("00604");
        result.setPrestador_codigoCnae(9313100);
        result.setPrestador_incentivadorCultural(2);
        result.setPrestador_codigoMunicipio(2927408);
        result.setPrestador_uf("BA");
        result.setPrestador_cep("40270150");
        result.setTomador_nome("CLEBER MORAES");
        result.setTomador_email("cleber@gmail.com");
        result.setTomador_cpf("82032530597");


        return result;
    }
    public RpsRequest createRequestAracaju() {

        RpsRequest result = new RpsRequest();
        result.setNdg_idPolinex(177);
        result.setNdg_nomeAcademia("ALPHA ARACAJU");
        result.setNdg_paymvId("987897-7897-7897-7897-7897987h989");
        result.setNdg_unitId("3529154c-4d07-45c4-9d64-5b4ea6415f2c");
        result.setRps_dataEmissao(new DateTime());
        result.setRps_dataCompetencia(new DateTime());
        result.setRps_discriminacaoServico("MENSALIDADE MUSCULAÇÃO");
        result.setRps_valorServicos(BigDecimal.valueOf(10.05d));
        result.setPrestador_cnpj("22769339000190");
        result.setPrestador_inscricaoMunicipal("1040093");
        //result.setPrestador_alicota(BigDecimal.valueOf(0.05d));
        result.setPrestador_naturezaOperacao(1);
        result.setPrestador_optanteSimplesNacional(2);
        result.setPrestador_itemListaServico("0604");
        result.setPrestador_codigoCnae(9313100);
        result.setPrestador_incentivadorCultural(2);
        result.setPrestador_codigoMunicipio(2800308);
        result.setPrestador_uf("SE");
        result.setPrestador_cep("49020200");
        result.setTomador_nome("CLEBER MORAES");
        result.setTomador_email("cleber@gmail.com");
        result.setTomador_cpf("82032530597");

        return result;
    }



    @Test
    public void geraRps() {
        List<RpsRequest> listPaymentsRequest = new ArrayList<>();

        listPaymentsRequest.add(createRequestSalvador());
        listPaymentsRequest.add(createRequestSalvador());
        listPaymentsRequest.add(createRequestSalvador());

        RpsRequest[] listRequest = new RpsRequest[]{createRequestSalvador()};

        RpsResponse pmResp = new RpsResponse();
        pmResp.setSelf(getBaseURL() + "/api/pagamento/");
        pmResp.setStartAt(0);
        pmResp.setMaxResults(1);
        pmResp.setTotal(1);
        pmResp.setItems(listRequest);

        Rps paymove = new Rps();

        paymove.getHttpClient().setBaseUrl(this.getBaseURL());

        List<RpsResponse> statusResponse = paymove.geraRps(listPaymentsRequest);
        assertNotNull(statusResponse);
    }

}
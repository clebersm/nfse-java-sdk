package br.com.polisystem.nfse.sdk.messages;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
abstract public class MessageHead<T>{

    private String self;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer startAt;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer maxResults;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer total;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String expand;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T[] items = null;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T fields = null;


    public String getSelf() {
        return self;
    }

    public MessageHead setSelf(String self) {
        this.self = self;
        return this;
    }

    public Integer getStartAt() {
        return startAt;
    }

    public MessageHead setStartAt(Integer startAt) {
        this.startAt = startAt;
        return this;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public MessageHead setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public Integer getTotal() {
        return total;
    }

    public MessageHead setTotal(Integer total) {
        this.total = total;
        return this;
    }

    public String getExpand() {
        return expand;
    }

    public MessageHead setExpand(String expand) {
        this.expand = expand;
        return this;
    }

    public T[] getItems() {
        return items;
    }

    public MessageHead setItems(T[] items) {
        this.items = items;
        return this;
    }

    public T getFields() {
        return fields;
    }

    public MessageHead setFields(T fields) {
        this.fields = fields;
        return this;
    }

    public String getId() {
        return id;
    }

    public MessageHead setId(String id) {
        this.id = id;
        return this;
    }
}

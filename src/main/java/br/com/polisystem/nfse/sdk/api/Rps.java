package br.com.polisystem.nfse.sdk.api;

import br.com.polisystem.nfse.sdk.messages.RpsRequest;
import br.com.polisystem.nfse.sdk.messages.RpsResponse;
import br.com.polisystem.nfse.sdk.messages.StatusResponse;
import br.com.polisystem.nfse.sdk.utils.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Rps extends AbstractAPI {

    //depurador
    private final Logger logger = LoggerFactory.getLogger(Rps.class);


    public List<RpsResponse> geraRps(List<RpsRequest> request) {
        HttpClient client = getHttpClient();
        return client.postForObject("/api/pagamento",request, List.class);
    }


}
package br.com.polisystem.nfse.sdk.api;

import br.com.polisystem.nfse.sdk.utils.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractAPI {

    private String baseURL = "https://192.168.80.157:44364";
    private String username = "";
    private String password = "";
    private HttpClient httpClient;

    public void setBaseUrl(String value) {
        this.baseURL = value;
    }

    public String getBaseUrl() {
        return this.baseURL;
    }

    public void setUsername(String value) {
        this.username = value;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return this.password;
    }

    public HttpClient getHttpClient() {

        if (httpClient == null){
            try {
                httpClient = new HttpClient();
            } catch (Exception e) {
                return null;
            }
        }

        return httpClient;
    }

    public void setHttpClient(HttpClient client) {
        httpClient = client;
    }
}

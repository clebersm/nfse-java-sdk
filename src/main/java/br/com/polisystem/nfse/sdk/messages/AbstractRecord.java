package br.com.polisystem.nfse.sdk.messages;

import com.fasterxml.jackson.annotation.JsonInclude;


abstract public class AbstractRecord <T>{

    private String self;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer startAt;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer maxResults;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer total;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String expand;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean isLast;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T[] items = null;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T fields = null;


    public String getSelf() {
        return self;
    }

    public AbstractRecord setSelf(String self) {
        this.self = self;
        return this;
    }

    public Integer getStartAt() {
        return startAt;
    }

    public AbstractRecord setStartAt(Integer startAt) {
        this.startAt = startAt;
        return this;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public AbstractRecord setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
        return this;
    }

    public Integer getTotal() {
        return total;
    }

    public AbstractRecord setTotal(Integer total) {
        this.total = total;
        return this;
    }

    public String getExpand() {
        return expand;
    }

    public AbstractRecord setIsLast(Boolean value) {
        this.isLast = value;
        return this;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public AbstractRecord setExpand(String expand) {
        this.expand = expand;
        return this;
    }

    public T[] getItems() {
        return items;
    }

    public AbstractRecord setItems(T[] items) {
        this.items = items;
        return this;
    }

    public T getFields() {
        return fields;
    }

    public AbstractRecord setFields(T fields) {
        this.fields = fields;
        return this;
    }

    public String getId() {
        return id;
    }

    public AbstractRecord setId(String id) {
        this.id = id;
        return this;
    }
}

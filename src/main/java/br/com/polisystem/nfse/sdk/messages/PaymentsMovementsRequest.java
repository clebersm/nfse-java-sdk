package br.com.polisystem.nfse.sdk.messages;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class PaymentsMovementsRequest {

    private Long id;
    private String ndg_paymvId;
    private String ndg_unitId;
    private Integer ndg_idPolinex;
    private String ndg_nomeAcademia;
    private Integer rps_sequenciaRps;
    private Integer rps_sequenciaLote;
    private Date rps_dataEmissao;
    private Date rps_dataCompetencia;
    private BigDecimal rps_valorServicos;
    private String rps_discriminacaoServico;
    private String rps_arquivo;
    private String rps_erro;
    private String rps_MensagemRetorno;
    private String prestador_razaoSocial;
    private String prestador_nome;
    private String prestador_cnpj;
    private String prestador_inscricaoMunicipal;
    private BigDecimal prestador_alicota;
    private Integer prestador_naturezaOperacao;
    private Integer prestador_regimeEspecialTributacao;
    private Integer prestador_optanteSimplesNacional;
    private Integer prestador_incentivadorCultural;
    private String prestador_itemListaServico;
    private Integer prestador_codigoCnae;
    private Integer prestador_codigoMunicipio;
    private String prestador_nomeCertificado;
    private String prestador_codigoTributacaoMunicipio;
    private String prestador_endereco;
    private String prestador_numero;
    private String prestador_bairro;
    private String prestador_cep;
    private String prestador_uf;
    private String prestador_email;
    private String prestador_telefone;
    private String tomador_nome;
    private String tomador_email;
    private String tomador_cpf;
    private String tomador_endereco;
    private String tomador_numero;
    private String tomador_bairro;
    private Integer tomador_cep;
    private String tomador_uf;
    private Integer tomador_codigoMunicipio;


    private Map<String, String> metadata = new LinkedHashMap();

    public PaymentsMovementsRequest() {

    }


    public void setId(Long id){ this.id = id;}

    public Long getId() { return id; }

    public void setNdg_idPolinex(Integer ndg_idPolinex){ this.ndg_idPolinex = ndg_idPolinex;}

    public Integer getNdg_idPolinex() { return ndg_idPolinex; }

    public void setNdg_nomeAcademia(String ndg_nomeAcademia){ this.ndg_nomeAcademia = ndg_nomeAcademia;}

    public String getNdg_nomeAcademia() { return ndg_nomeAcademia; }

    public void setNdg_paymvId(String ndg_paymvId){ this.ndg_paymvId = ndg_paymvId;}

    public String getNdg_paymvId() { return ndg_paymvId; }

    public void setNdg_unitId(String ndg_unitId){ this.ndg_unitId = ndg_unitId;}

    public String getNdg_unitId() { return ndg_unitId; }

    public void setPrestador_alicota(BigDecimal prestador_alicota){ this.prestador_alicota = prestador_alicota;}

    public BigDecimal getPrestador_alicota() { return prestador_alicota; }

    public void setPrestador_bairro(String prestador_bairro){ this.prestador_bairro = prestador_bairro;}

    public String getPrestador_bairro() { return prestador_bairro; }

    public void setPrestador_cep(String prestador_cep){ this.prestador_cep = prestador_cep;}

    public String getPrestador_cep() { return prestador_cep; }

    public void setPrestador_cnpj(String prestador_cnpj){ this.prestador_cnpj = prestador_cnpj;}

    public String getPrestador_cnpj() { return prestador_cnpj; }

    public void setPrestador_codigoCnae(Integer prestador_codigoCnae){ this.prestador_codigoCnae = prestador_codigoCnae;}

    public Integer getPrestador_codigoCnae() { return prestador_codigoCnae; }

    public void setPrestador_codigoMunicipio(Integer prestador_codigoMunicipio){ this.prestador_codigoMunicipio = prestador_codigoMunicipio;}

    public Integer getPrestador_codigoMunicipio() { return prestador_codigoMunicipio; }

    public void setPrestador_codigoTributacaoMunicipio(String prestador_codigoTributacaoMunicipio){ this.prestador_codigoTributacaoMunicipio = prestador_codigoTributacaoMunicipio;}

    public String getPrestador_codigoTributacaoMunicipio() { return prestador_codigoTributacaoMunicipio; }

    public void setPrestador_email(String prestador_email){ this.prestador_email = prestador_email;}

    public String getPrestador_email() { return prestador_email; }

    public void setPrestador_endereco(String prestador_endereco){ this.prestador_endereco = prestador_endereco;}

    public String getPrestador_endereco() { return prestador_endereco; }

    public void setPrestador_incentivadorCultural(Integer prestador_incentivadorCultural){ this.prestador_incentivadorCultural = prestador_incentivadorCultural;}

    public Integer getPrestador_incentivadorCultural() { return prestador_incentivadorCultural; }

    public void setPrestador_inscricaoMunicipal(String prestador_inscricaoMunicipal){ this.prestador_inscricaoMunicipal = prestador_inscricaoMunicipal;}

    public String getPrestador_inscricaoMunicipal() { return prestador_inscricaoMunicipal; }

    public void setPrestador_itemListaServico(String prestador_itemListaServico){ this.prestador_itemListaServico = prestador_itemListaServico;}

    public String getPrestador_itemListaServico() { return prestador_itemListaServico; }

    public void setPrestador_naturezaOperacao(Integer prestador_naturezaOperacao){ this.prestador_naturezaOperacao = prestador_naturezaOperacao;}

    public Integer getPrestador_naturezaOperacao() { return prestador_naturezaOperacao; }

    public void setPrestador_nome(String prestador_nome){ this.prestador_nome = prestador_nome;}

    public String getPrestador_nome() { return prestador_nome; }

    public void setPrestador_nomeCertificado(String prestador_nomeCertificado){ this.prestador_nomeCertificado = prestador_nomeCertificado;}

    public String getPrestador_nomeCertificado() { return prestador_nomeCertificado; }

    public void setPrestador_numero(String prestador_numero){ this.prestador_numero = prestador_numero;}

    public String getPrestador_numero() { return prestador_numero; }

    public void setPrestador_optanteSimplesNacional(Integer prestador_optanteSimplesNacional){ this.prestador_optanteSimplesNacional = prestador_optanteSimplesNacional;}

    public Integer getPrestador_optanteSimplesNacional() { return prestador_optanteSimplesNacional; }

    public void setPrestador_razaoSocial(String prestador_razaoSocial){ this.prestador_razaoSocial = prestador_razaoSocial;}

    public String getPrestador_razaoSocial() { return prestador_razaoSocial; }

    public void setPrestador_regimeEspecialTributacao(Integer prestador_regimeEspecialTributacao){ this.prestador_regimeEspecialTributacao = prestador_regimeEspecialTributacao;}

    public Integer getPrestador_regimeEspecialTributacao() { return prestador_regimeEspecialTributacao; }

    public void setPrestador_telefone(String prestador_telefone){ this.prestador_telefone = prestador_telefone;}

    public String getPrestador_telefone() { return prestador_telefone; }

    public void setPrestador_uf(String prestador_uf){ this.prestador_uf = prestador_uf;}

    public String getPrestador_uf() { return prestador_uf; }

    public void setRps_arquivo(String rps_arquivo){ this.rps_arquivo = rps_arquivo;}

    public String getRps_arquivo() { return rps_arquivo; }

    public void setRps_dataCompetencia(Date rps_dataCompetencia){ this.rps_dataCompetencia = rps_dataCompetencia;}

    public Date getRps_dataCompetencia() { return rps_dataCompetencia; }

    public void setRps_dataEmissao(Date rps_dataEmissao){ this.rps_dataEmissao = rps_dataEmissao;}

    public Date getRps_dataEmissao() { return rps_dataEmissao; }

    public void setRps_discriminacaoServico(String rps_discriminacaoServico){ this.rps_discriminacaoServico = rps_discriminacaoServico;}

    public String getRps_discriminacaoServico() { return rps_discriminacaoServico; }

    public void setRps_erro(String rps_erro){ this.rps_erro = rps_erro;}

    public String getRps_erro() { return rps_erro; }

    public void setRps_MensagemRetorno(String rps_MensagemRetorno){ this.rps_MensagemRetorno = rps_MensagemRetorno;}

    public String getRps_MensagemRetorno() { return rps_MensagemRetorno; }

    public void setRps_sequenciaLote(Integer rps_sequenciaLote){ this.rps_sequenciaLote = rps_sequenciaLote;}

    public Integer getRps_sequenciaLote() { return rps_sequenciaLote; }

    public void setRps_sequenciaRps(Integer rps_sequenciaRps){ this.rps_sequenciaRps = rps_sequenciaRps;}

    public Integer getRps_sequenciaRps() { return rps_sequenciaRps; }

    public void setRps_valorServicos(BigDecimal rps_valorServicos){ this.rps_valorServicos = rps_valorServicos;}

    public BigDecimal getRps_valorServicos() { return rps_valorServicos; }

    public void setTomador_bairro(String tomador_bairro){ this.tomador_bairro = tomador_bairro;}

    public String getTomador_bairro() { return tomador_bairro; }

    public void setTomador_cep(Integer tomador_cep){ this.tomador_cep = tomador_cep;}

    public Integer getTomador_cep() { return tomador_cep; }

    public void setTomador_codigoMunicipio(Integer tomador_codigoMunicipio){ this.tomador_codigoMunicipio = tomador_codigoMunicipio;}

    public Integer getTomador_codigoMunicipio() { return tomador_codigoMunicipio; }

    public void setTomador_cpf(String tomador_cpf){ this.tomador_cpf = tomador_cpf;}

    public String getTomador_cpf() { return tomador_cpf; }

    public void setTomador_email(String tomador_email){ this.tomador_email = tomador_email;}

    public String getTomador_email() { return tomador_email; }

    public void setTomador_endereco(String tomador_endereco){ this.tomador_endereco = tomador_endereco;}

    public String getTomador_endereco() { return tomador_endereco; }

    public void setTomador_nome(String tomador_nome){ this.tomador_nome = tomador_nome;}

    public String getTomador_nome() { return tomador_nome; }

    public void setTomador_numero(String tomador_numero){ this.tomador_numero = tomador_numero;}

    public String getTomador_numero() { return tomador_numero; }

    public void setTomador_uf(String tomador_uf){ this.tomador_uf = tomador_uf;}

    public String getTomador_uf() { return tomador_uf; }



    public String toString() {
        StringBuilder SQL = new StringBuilder();
        SQL.append("TransactionsRequest {");
        SQL.append("Id =" + getId());
        SQL.append(",ndg_paymvId =" + getNdg_paymvId());
        SQL.append(",ndg_unitId =" + getNdg_unitId());
        SQL.append(",ndg_idPolinex =" + getNdg_idPolinex());
        SQL.append(",ndg_nomeAcademia =" + getNdg_nomeAcademia());
        SQL.append(",rps_sequenciaRps =" + getRps_sequenciaRps());
        SQL.append(",rps_sequenciaLote =" + getRps_sequenciaLote());
        SQL.append(",rps_dataEmissao =" + getRps_dataEmissao());
        SQL.append(",rps_dataCompetencia =" + getRps_dataCompetencia());
        SQL.append(",rps_valorServicos =" + getRps_valorServicos());
        SQL.append(",rps_discriminacaoServico =" + getRps_discriminacaoServico());
        SQL.append(",rps_arquivo =" + getRps_arquivo());
        SQL.append(",rps_erro =" + getRps_erro());
        SQL.append(",rps_MensagemRetorno =" + getRps_MensagemRetorno());
        SQL.append(",prestador_razaoSocial =" + getPrestador_razaoSocial());
        SQL.append(",prestador_nome =" + getPrestador_nome());
        SQL.append(",prestador_cnpj =" + getPrestador_cnpj());
        SQL.append(",prestador_inscricaoMunicipal =" + getPrestador_inscricaoMunicipal());
        SQL.append(",prestador_alicota =" + getPrestador_alicota());
        SQL.append(",prestador_naturezaOperacao =" + getPrestador_naturezaOperacao());
        SQL.append(",prestador_regimeEspecialTributacao =" + getPrestador_regimeEspecialTributacao());
        SQL.append(",prestador_optanteSimplesNacional =" + getPrestador_optanteSimplesNacional());
        SQL.append(",prestador_incentivadorCultural =" + getPrestador_incentivadorCultural());
        SQL.append(",prestador_itemListaServico =" + getPrestador_itemListaServico());
        SQL.append(",prestador_codigoCnae =" + getPrestador_codigoCnae());
        SQL.append(",prestador_codigoMunicipio =" + getPrestador_codigoMunicipio());
        SQL.append(",prestador_nomeCertificado =" + getPrestador_nomeCertificado());
        SQL.append(",prestador_codigoTributacaoMunicipio =" + getPrestador_codigoTributacaoMunicipio());
        SQL.append(",prestador_endereco =" + getPrestador_endereco());
        SQL.append(",prestador_numero =" + getPrestador_numero());
        SQL.append(",prestador_bairro =" + getPrestador_bairro());
        SQL.append(",prestador_cep =" + getPrestador_cep());
        SQL.append(",prestador_uf =" + getPrestador_uf());
        SQL.append(",prestador_email =" + getPrestador_email());
        SQL.append(",prestador_telefone =" + getPrestador_telefone());
        SQL.append(",tomador_nome =" + getTomador_nome());
        SQL.append(",tomador_email =" + getTomador_email());
        SQL.append(",tomador_cpf =" + getTomador_cpf());
        SQL.append(",tomador_endereco =" + getTomador_endereco());
        SQL.append(",tomador_numero =" + getTomador_numero());
        SQL.append(",tomador_bairro =" + getTomador_bairro());
        SQL.append(",tomador_cep =" + getTomador_cep());
        SQL.append(",tomador_uf =" + getTomador_uf());
        SQL.append(",tomador_codigoMunicipio =" + getTomador_codigoMunicipio());
        SQL.append("}");

        return SQL.toString();
    }
}

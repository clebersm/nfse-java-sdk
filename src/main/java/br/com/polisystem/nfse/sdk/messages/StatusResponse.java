package br.com.polisystem.nfse.sdk.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StatusResponse<T> {
    private int code;
    private T object;
    private List<String> messages;

    protected String getHTTPResponses(int code){
        switch(code){
            case 200: return "OK";
            case 201: return "Created";
            case 202 : return "Accepted";
            case 203 : return "Non-Authoritative Information (since HTTP/1.1)";
            case 204 : return "No Content";
            case 205 : return "Reset Content";
            case 206 : return "Partial Content (RFC 7233)";
            case 207 : return "Multi-Status (WebDAV; RFC 4918)";
            case 208 : return "Already Reported (WebDAV; RFC 5842)";
            case 226 : return "IM Used (RFC 3229)";
            case 300 : return "Multiple Choices";
            case 301 : return "Moved Permanently";
            case 302 : return "Found";
            case 303 : return "See Other (since HTTP/1.1)";
            case 304 : return "Not Modified (RFC 7232)";
            case 305 : return "Use Proxy (since HTTP/1.1)";
            case 306 : return "Switch Proxy";
            case 307 : return "Temporary Redirect (since HTTP/1.1)";
            case 308 : return "Permanent Redirect (RFC 7538)";
            case 400 : return "Bad Request";
            case 401 : return "Unauthorized (RFC 7235)";
            case 402 : return "PaymentRequest Required";
            case 403 : return "Forbidden";
            case 404 : return "Not Found";
            case 405 : return "Method Not Allowed";
            case 406 : return "Not Acceptable";
            case 407 : return "Proxy Authentication Required (RFC 7235)";
            case 408 : return "Request Timeout";
            case 409 : return "Conflict";
            case 410 : return "Gone";
            case 411 : return "Length Required";
            case 412 : return "Precondition Failed (RFC 7232)";
            case 413 : return "Payload Too Large (RFC 7231)";
            case 414 : return "URI Too Long (RFC 7231)";
            case 415 : return "Unsupported Media Type";
            case 416 : return "Range Not Satisfiable (RFC 7233)";
            case 417 : return "Expectation Failed";
            case 418 : return "I'm a teapot (RFC 2324)";
            case 421 : return "Misdirected Request (RFC 7540)";
            case 422 : return "Unprocessable Entity (WebDAV; RFC 4918)";
            case 423 : return "Locked (WebDAV; RFC 4918)";
            case 424 : return "Failed Dependency (WebDAV; RFC 4918)";
            case 426 : return "Upgrade Required";
            case 428 : return "Precondition Required (RFC 6585)";
            case 429 : return "Too Many Requests (RFC 6585)";
            case 431 : return "Request Header Fields Too Large (RFC 6585)";
            case 451 : return "Unavailable For Legal Reasons (RFC 7725)";
            case 500 : return "Internal Server Error";
            case 501 : return "Not Implemented";
            case 502 : return "Bad Gateway";
            case 503 : return "Service Unavailable";
            case 504 : return "Gateway Timeout";
            case 505 : return "HTTP Version Not Supported";
            case 506 : return "Variant Also Negotiates (RFC 2295)";
            case 507 : return "Insufficient Storage (WebDAV; RFC 4918)";
            case 508 : return "Loop Detected (WebDAV; RFC 5842)";
            case 510 : return "Not Extended (RFC 2774)";
            case 511 : return "Network Authentication Required (RFC 6585)";
        }

        return "";
    }

    public StatusResponse(int status){
        this.code = status;
        this.messages = new ArrayList<>();
        this.messages.add(getHTTPResponses(code));
    }

    public StatusResponse(int status,List<String> message){
        this.code = status;
        this.messages = message;
    }

    @JsonCreator
    public StatusResponse(@JsonProperty("code") int status,@JsonProperty("messages") List<String> message,@JsonProperty("object") T object){
        this.code = status;
        this.messages = message;
        this.object = object;
    }

    public StatusResponse(int status, String message){
        this.code = status;
        this.messages = new ArrayList<>();
        this.messages.add(message);
    }

    public StatusResponse(int status, String message,T object){
        this.code = status;
        this.messages = new ArrayList<>();
        this.messages.add(message);
        this.object = object;
    }

    public StatusResponse(int status, Exception exception){
        this.code = status;
        this.messages = new ArrayList<>();
        this.messages.add(exception.getMessage());
    }

    public StatusResponse(){
        this.code = 200;
        this.messages = new ArrayList<>();
        this.messages.add(getHTTPResponses(code));
    }

    public int getCode() {
        return code;
    }

    public StatusResponse setCode(int value) {
        code = value;
        return this;
    }

    public List<String> getMessage() {
        return messages;
    }

    public StatusResponse setMessage(List<String> value) {
        messages = value;
        return this;
    }

    public T getObject() {
        return object;
    }

    public StatusResponse setObject(T value) {
        object = value;
        return this;
    }

}

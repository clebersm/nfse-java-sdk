package br.com.polisystem.nfse.sdk.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public class HttpClient {

    private final Logger logger = LoggerFactory.getLogger(HttpClient.class);
    protected String baseURL;
    protected String username;
    protected String password;

    protected String toJSON(Object value) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        try {
            // Convert object to JSON string
            String jsonInString = mapper.writeValueAsString(value);
            return jsonInString;

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setBaseUrl(String value) {
        this.baseURL = value;
    }

    public String getBaseUrl() {
        return this.baseURL;
    }

    public void setUsername(String value) {
        this.username = value;
    }

    public String getUsername() {
        return this.username;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getPassword() {
        return this.password;
    }

    protected CredentialsProvider getCredentialsProvider(String username, String password) {
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(username, password);
        provider.setCredentials(AuthScope.ANY, credentials);

        return provider;
    }

    protected CloseableHttpClient createAcceptSelfSignedCertificateClient()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

        // use the TrustSelfSignedStrategy to allow Self Signed Certificates
        SSLContext sslContext = SSLContextBuilder
                .create()
                .loadTrustMaterial(new TrustSelfSignedStrategy())
                .build();

        // we can optionally disable hostname verification.
        // if you don't want to further weaken the security, you don't have to include this.
        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();

        // create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
        // and allow all hosts verifier.
        SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);

        Optional<String> username = Optional.ofNullable(getUsername());
        Optional<String> password = Optional.ofNullable(getPassword());

        if (username.isPresent() && password.isPresent()) {
            return HttpClients
                    .custom()
                    .setSSLSocketFactory(connectionFactory)
                    .setDefaultCredentialsProvider(getCredentialsProvider(username.get(), password.get()))
                    .build();
        }

        // finally create the HttpClient using HttpClient factory methods and assign the ssl socket factory
        return HttpClients
                .custom()
                .setSSLSocketFactory(connectionFactory)
                .build();
    }

    protected CloseableHttpClient getHttpClient(boolean ssl) {
        if (ssl) {
            try {
                return createAcceptSelfSignedCertificateClient();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            }
        }
        return HttpClients.createDefault();
    }

    public String doGet(String url) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        url = getBaseUrl() + url;

        CloseableHttpClient httpclient = getHttpClient(url.startsWith("https:"));
        try {

            HttpGet httpget = new HttpGet(url);

            logger.debug("Executing request " + httpget.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            String responseBody = httpclient.execute(httpget, responseHandler);
            logger.debug("doGet.response : " + responseBody);
            return responseBody;
        } finally {
            httpclient.close();
        }
    }

    public <T> T getForObject(String url, java.lang.Class<T> responseType, java.lang.Object... uriVariables) {
        url = String.format(url, uriVariables);
        try {
            String response = doGet(url);
            if (response != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response, responseType);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String doPut(String url, Object value) throws IOException {

        url = getBaseUrl() + url;

        CloseableHttpClient httpclient = getHttpClient(url.startsWith("https:"));
        try {

            String payload = toJSON(value);
            logger.debug("doPut.request : " + payload);

            HttpPut httpput = new HttpPut(url);
            StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);
            httpput.setEntity(entity);

            logger.debug("Executing request " + httpput.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            String responseBody = httpclient.execute(httpput, responseHandler);
            logger.debug("doPut.response : " + responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.close();
        }

        return null;
    }

    public <T> T putForObject(String url, Object payload, java.lang.Class<T> responseType, java.lang.Object... uriVariables) {
        url = String.format(url, uriVariables);
        String response = null;
        try {
            response = doPut(url, payload);
            if (response != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response, responseType);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String doPost(String url, Object value) throws IOException {
        url = getBaseUrl() + url;

        CloseableHttpClient httpclient = getHttpClient(url.startsWith("https:"));
        try {

            String payload = toJSON(value);
            logger.debug("doPost.request : " + payload);

            HttpPost httppost = new HttpPost(url);
            StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_JSON);
            httppost.setEntity(entity);

            logger.debug("Executing request " + httppost.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            String responseBody = httpclient.execute(httppost, responseHandler);
            logger.debug("doPost.response : " + responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.close();
        }

        return null;
    }

    public <T> T postForObject(String url, Object payload, java.lang.Class<T> responseType, java.lang.Object... uriVariables) {
        url = String.format(url, uriVariables);
        String response = null;
        try {
            response = doPost(url, payload);
            if (response != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response, responseType);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String doDelete(String url) throws IOException {
        url = getBaseUrl() + url;

        CloseableHttpClient httpclient = getHttpClient(url.startsWith("https:"));
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            logger.debug("Executing request " + httpDelete.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            String responseBody = httpclient.execute(httpDelete, responseHandler);
            logger.debug("doDelete.response : " + responseBody);
            return responseBody;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpclient.close();
        }

        return null;
    }

    public <T> T deleteForObject(String url, java.lang.Class<T> responseType, java.lang.Object... uriVariables) {
        url = String.format(url, uriVariables);
        String response = null;
        try {
            response = doDelete(url);
            if (response != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response, responseType);
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
